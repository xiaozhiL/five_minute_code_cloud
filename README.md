# 五分钟码云

#### 介绍
连玉君老师录制的码云平台简介，助你快速了解码云，开始你的第一个项目。

#### 使用说明
[连玉君-码云简介](https://www.zhihu.com/zvideo/1216422793992175616?utm_source=wechat_session&utm_medium=social&utm_oi=757639800307060736)

#### 加速播放
知乎好像不支持加速播放，如果你是谷歌浏览器，参考以下方法：

只要是html5的视频标签都可以调，用Chrome console 注入JS即可
* 1. 用Chrome打开视频网页
* 2. 右键->检查->console
* 3. 输入（2倍加速）
*  $("video").playbackRate=2

